# EOPEN Hackathon 2019

This repository contains the material used during the **EO Big Data Shift Hackathon** held on November 7 and 8, 2019 in Frascati, Italy.

The root folder contains the following presentation slides:

1. `EOPEN-Hackathon-2019-1-Intro.pdf` introduces the EOPEN project and give an overview of the exercises.
2. `EOPEN-Hackathon-2019-2-Exercises.pdf` provides more technical information about the EOPEN Platform and describes the exercises 1, 2 and 4 in details.
3. `EOPEN-Hackathon-2019-3-Exercise-3.pdf` focuses on exercise 3.
4. `EOPEN-Hackathon-2019-4-Feedback.pdf` provides concluding remarks.

Data files required by the exercises can be found in the folders `tweets`, `better` and `openeo`.

## Platform Credentials

URL: [https://proto2.eopen.spaceapplications.com](https://proto2.eopen.spaceapplications.com)
User: hackathon

## Exercise 1 - BETTER  PoC

Steps

1. Download the BETTER client script already formatted as a process wrapper.

2. Edit locally the script and customize the process (e.g. set the version to your initials)

3. Login to the EOPEN Platform (credentials provided during the session)

4. Access the process importer page

5. Import the script

6. Create a new workflow (in the "Hackathon" workspace) and give it a unique name and version

   a. add your process

   b. add the "Sentinel-2 Watermask Generation" process and connect it after the first one (taking the parameter names into account)

7. Execute your workflow (see the example below for some default value)

8. When the execution is complete, access the execution report then the output files

Data files may be found in the `better` sub-folder.

[Result example E1#1](https://proto2.eopen.spaceapplications.com/webui/user/order?id=146)

## Exercise 2 - OpenEO PoC

Same as Exercice 1 with OpenEO client script

Data files may be found in the `openeo` sub-folder.

[Result example E2#1](https://proto2.eopen.spaceapplications.com/webui/user/order?id=129)

## Exercise 3 - Access to Tweets data

In this exercise, an EOPEN service is used to access Twitter content in JSON and RDF.

Exercise details are provided in `EOPEN-Hackathon-2019-3-Exercise-3.pdf`.

Data files may be found in the `tweets` sub-folder.

## Exercise 4 - Multi-platform execution (incl. ONDA DIAS)

Objective: Run a process in the ONDA DIAS environment and benefit from the direct access to the archive data files.

Steps

1. Use the ONDA DIAS OData API to search for Sentinel-2 products (AOI and TOI)

   Example request:

   ```url
   https://catalogue.onda-dias.eu/dias-catalogue/Products?$search="name:S1?_S?_SLC_* AND footprint:"Intersects(POLYGON((128.0245 51.4190, 128.3101 51.4177, 128.3378 51.6290, 128.0100 51.6275, 128.0245 51.4190)))" AND beginPosition:[2019-06-01T00:00:00.000Z TO 2019-06-30T23:59:59.999Z]"&$format=json&$top=50&$skip=0&$select=id,name,creationDate,beginPosition,offline,size,pseudopath,footprint,downloadable
   ```

   Create your own polygon e.g. using [Wicket](https://arthur-e.github.io/Wicket/sandbox-gmaps3.html)

2. Select a matching product and take its `name` and `pseudo path` attributes. This gives direct access to the product files without requiring a download step

3. Build an absolute path as follows: `/data/inputs/<pseudo_path>/<name>`

   Example path: `/data/inputs/S2/2B/LEVEL-2A/S2MSI2A/2019/06/30/S2B_MSIL2A_20190630T022559_N0212_R046_T52UDC_20190630T045620.zip`

4. Open [workflow #53](https://proto2.eopen.spaceapplications.com/knowledge/processorEditor/53) and inspect it: the only process is configured to run in ONDA DIAS

5. Execute the workflow on the product you have selected above

6. When the execution is complete, access the execution report then the output files

[Result example E4#1](https://proto2.eopen.spaceapplications.com/webui/user/order?id=122)

-------------------------------------------------------------------------------------------

## Exercise 5 - Import and execute your own algorithm

Code your own algorithm and import it!

#!/usr/bin/env python2
# coding: utf-8

import logging
import requests
from urlparse import urlparse
from osgeo import gdal
from lxml import etree
from StringIO import StringIO
import os

logger = logging.getLogger(__name__)


def get_vsi_url(enclosure, username, api_key):
    parsed_url = urlparse(enclosure)
    url = '/vsicurl/{}://{}:{}@{}/api{}'.format(list(parsed_url)[0],
                                            username,
                                            api_key,
                                            list(parsed_url)[1],
                                            list(parsed_url)[2])
    return url


def vsi_download(out_dir, enclosure, bbox, username, api_key, bands, bands_desc):
    vsi_url = get_vsi_url(enclosure, username, api_key)
    ulx, uly, lrx, lry = bbox[0], bbox[3], bbox[2], bbox[1]
    res = []
    for band in bands:
        file_name = os.path.split(urlparse(enclosure).path)[1]
        file_base, file_ext = os.path.splitext(file_name)
        out_file = '{}_subset_{}{}'.format(file_base, bands_desc.get(band, 'B--'), file_ext)
        output = os.path.join(out_dir, out_file)
        logger.info('Downloading band data in file: %s', output)
        ds = gdal.Open(vsi_url)
        ds = gdal.Translate(destName=output,
                            srcDS=ds,
                            projWin = [ulx, uly, lrx, lry],
                            projWinSRS = 'EPSG:4326',
                            outputType=gdal.GDT_Float32,
                            bandList=[band])
        ds = None
        res.append(out_file)
    return res


def execute(out_dir, user, api_url, api_key, bands, bbox_string, temporal_extent):
    """
    Identification:
    Name -- BETTER PoC
    Description -- Retrieve Sentinel 2 bands in GeoTIFF
    Version -- put-your-initial-here
    Author -- Space Applications Services
    Mission -- hackathon
    
    Inputs:
    user -- User -- 45/User String
    api_url -- API URL -- 45/User String
    api_key -- API Key -- 45/User String
    bands -- Bands (e.g. '3 11') -- 45/User String
    bbox_string -- BBox (e.g. 'min_x, min_y, max_x, max_y') -- 45/User String
    temporal_extent -- Temporal Extent (ISO: '<start>/<end>') -- 44/DateRange
    
    Outputs:
    band_dir -- Band files directory -- 45/User String
    band_files -- Band files -- 45/User String
    
    Main Dependency:
    python-2

    Software Dependencies:
    python-2
    gdal-x.y
    
    Processing Resources:
    ram -- 1
    disk -- 10
    cpu -- 1
    """

    bbox = bbox_string.split(',')
    start_date, stop_date = temporal_extent.split('/')

    params = {
        'apikey': api_key,
        'bbox': ','.join([str(coord) for coord in bbox]),
        'cat': '{!dataitem}', 
        'start': start_date, 
        'stop': stop_date
    }

    r = requests.get(api_url, params=params)
    logger.info('Search result code: %s', r.status_code)
    logger.debug('Search result content: %s', r.content)

    namespaces = {
        'atom': 'http://www.w3.org/2005/Atom',
        'opt': 'http://www.opengis.net/opt/2.1',
        'om':  'http://www.opengis.net/om/2.0',
        'gml': 'http://www.opengis.net/gml/3.2',
        'eop': 'http://www.opengis.net/eop/2.1',
        'sar': 'http://www.opengis.net/sar/2.1',
        'ssp': 'http://www.opengis.net/ssp/2.1'
    }

    for key, value in namespaces.items():
        etree.register_namespace(key, value)

    tree = etree.parse(StringIO(r.content))

    enclosures = []

    for index, entry in enumerate(tree.xpath('/atom:feed/atom:entry', namespaces=namespaces)):
        enclosures.append(entry.xpath('atom:link[@rel="enclosure" and @type="image/tiff"]/@href', namespaces=namespaces)[0])
        #.attrib('href')

    logger.info('Enclosures (%s): %s', len(enclosures), enclosures)


    src_ds = gdal.Open(get_vsi_url(enclosures[6], user, api_key))

    bands_desc = {}
    for band in range(src_ds.RasterCount):
        band += 1
        srcband = src_ds.GetRasterBand(band)
        logger.info('band index {} - {}'.format(band, srcband.GetDescription()))
        bands_desc[str(band)] = srcband.GetDescription()

    local_path = vsi_download(out_dir, enclosures[6], bbox, user, api_key, bands.split(' '), bands_desc)
    logger.info('Band files: %s', local_path)
    
    return {
        "band_dir": out_dir,
        "band_files": local_path
    }


if __name__ == "__main__":
    formatter = logging.Formatter(fmt="%(levelname)s - %(message)s")
    handlers = [logging.FileHandler("/tmp/process-wrapper.log"), logging.StreamHandler()]
    for handler in handlers:
        handler.setFormatter(formatter)
    level = logging.getLevelName(logging.DEBUG)
    logger.setLevel(level)
    for handler in handlers:
        logger.addHandler(handler)
    result = execute(
        out_dir="/tmp",
        user="bvalentin",
        api_url="https://catalog.terradue.com/ard-s2-boa-reflectances/search",
        api_key="AKCp5e2qc4Uw1nQL54D6ribJzR69TiHH4Ld2Buwookbb2XdcL9FbLBUAXCQbpseC3wHwKMtXa",
        bands="3 11 13",
        bbox_string="-70.255, -12.855, -70.098, -12.718", # min_x, min_y, max_x, max_y
        #temporal_extent="2019-04-01T00:00:00Z/2019-05-01T23:59:59Z"
        temporal_extent="2019-04-01/2019-05-01"
    )
    print("result: ", result)
